# README

# Development environment
    
* ruby 2.6.3p62
* rails 2.6.3
* angular 8.2.14
* node v13.2.0
* npm 6.4.1

# Assumptions
* Assuming multiple employees can review one employee
